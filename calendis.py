from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
import datetime
import smtplib

emailString = """\
Subject: Calendis schedule

Date Hour"""
email_str_len = len(emailString)

def iterate_over_weeks(start, end, driver, appointment, begindate):
    global emailString
    for i in range (start, end):
        # print(i)
        xpath = '//*[@id="appointment-calendar"]/div[2]/div[2]/div[' + str(i) + ']/div/div[1]'
        # print(xpath)
        day = driver.find_element_by_xpath(xpath)
        day.click()
        time.sleep(3)
        children = appointment.find_elements_by_class_name('slot-time')
        for child in children:
            if (is_after_a_hour(20, child.text.split(':')[0])):
                # print(child.text.split(':')[0])
                emailString = emailString + "\n" + str(begindate + i - 1) + " " + child.text.split(':')[0]


def is_after_a_hour(expected_hour, child_hour):
    return expected_hour <= int(child_hour)

user = ""
pwd = ""

driver = webdriver.Chrome(executable_path='D:\justforfun\chromedriver_win32\chromedriver')
driver.get('https://www.calendis.ro/cluj-napoca/baza-sportiva-gheorgheni/tenis-cu-peretele/s')
#driver.maximize_window()
#assert "Facebook" in driver.title
elem = driver.find_element_by_id("forEmail")
elem.send_keys(user)
elem = driver.find_element_by_id("forPassword")
elem.send_keys(pwd)
elem.send_keys(Keys.RETURN)
time.sleep(4)
appointment = driver.find_element_by_id('appointment-slots')

# iterate over the current week
# today's number
today_number = datetime.datetime.today().weekday() + 1
today_day_number = datetime.datetime.today().day
how_more_day_on_the_week = 8 - today_number
print("Today weekday nr: " + str(today_number))
print("Today: " + str(today_day_number))
'''
days = driver.find_element_by_xpath('//*[@id="appointment-calendar"]/div[2]/div[2]')
day_active_date = driver.find_elements_by_class_name("calendis-days")
print(day_active_date)
for child in day_active_date:
   print(child.text)
day_active_date_number = day_active_date[0].find_element_by_class_name('day-nr')
print("Selected date: " + str(day_active_date_number[0].text))
'''
# iterate over the current week
iterate_over_weeks(today_number, 8, driver, appointment, today_day_number)

# iterate over the next week, first click to next
next = driver.find_element_by_xpath('//*[@id="appointment-calendar"]/div[2]/div[3]/div')
next.click()
iterate_over_weeks(1, 8, driver, appointment, today_day_number + how_more_day_on_the_week)

# iterate over the next-next week
next.click()
iterate_over_weeks(1, today_number + 1, driver, appointment, today_day_number + how_more_day_on_the_week + 7)
print(emailString)
driver.close()

# TODO: if exists something!!! 
# TODO: beauty mail
if len(emailString) > email_str_len:
    try:
        server = smtplib.SMTP('smtp.gmail.com:587')
        server.ehlo()
        server.starttls()
        print("Here")
        server.login("", "")
        server.sendmail("", "", emailString)
        print("Email sent")
        server.quit()
    except:
        print("Email failed to send")